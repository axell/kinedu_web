Rails.application.routes.draw do
  
  
 devise_for :users 
  
  authenticate :user do
    
    resources :babies
    resources :assistants
    resources :activity_logs
    resources :activities

    get 'search/create'
    root to:'welcome#index'
    
  end


  unauthenticated :user do
    root to:'welcome#unregistered'
  end
  
end
