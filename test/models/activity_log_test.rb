# == Schema Information
#
# Table name: activity_logs
#
#  id           :integer          not null, primary key
#  comments     :text(65535)
#  duration     :integer
#  start_time   :datetime
#  status       :string(255)      default("started")
#  stop_time    :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  activity_id  :integer
#  assistant_id :integer
#  baby_id      :integer
#
# Indexes
#
#  index_activity_logs_on_activity_id   (activity_id)
#  index_activity_logs_on_assistant_id  (assistant_id)
#  index_activity_logs_on_baby_id       (baby_id)
#
# Foreign Keys
#
#  fk_rails_...  (activity_id => activities.id)
#  fk_rails_...  (assistant_id => assistants.id)
#  fk_rails_...  (baby_id => babies.id)
#
require 'test_helper'

class ActivityLogTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
