class AddStatusToActivityLogs < ActiveRecord::Migration[5.2]
  def change
    add_column :activity_logs, :status, :string,default:"started"
  end
end
