# == Schema Information
#
# Table name: activity_logs
#
#  id           :integer          not null, primary key
#  comments     :text(65535)
#  duration     :integer
#  start_time   :datetime
#  status       :string(255)      default("started")
#  stop_time    :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  activity_id  :integer
#  assistant_id :integer
#  baby_id      :integer
#
# Indexes
#
#  index_activity_logs_on_activity_id   (activity_id)
#  index_activity_logs_on_assistant_id  (assistant_id)
#  index_activity_logs_on_baby_id       (baby_id)
#
# Foreign Keys
#
#  fk_rails_...  (activity_id => activities.id)
#  fk_rails_...  (assistant_id => assistants.id)
#  fk_rails_...  (baby_id => babies.id)
#
class ActivityLog < ApplicationRecord
  belongs_to :baby
  belongs_to :assistant
  belongs_to :activity

  #states machines for activity logs
  include AASM

  aasm column: "status" do
  	
    state :started, initial: true
    state :finished 

    event :finish do  
      transitions from: :started, to: :finished
    end

  end


  validates :comments,:duration,:stop_time , presence:true , on: :update
  validates :start_time,presence:true, on: :create

  #finalize activity if this in complete
  def finish_log
    time = stop_time - start_time if stop_time
    mins = (time / 1.minute).round if time
    if mins == duration 
      self.finish! if status == "started"
    end
  end

  #set default values for duration and stoptime
  def set_default_duration_and_stop_time
    self.stop_time ||= Time.now
    self.duration ||= 0
  end
  
  #set duration of activity
  def set_duration
    time = stop_time - start_time
    time = (time / 1.minute).round
    self.update(duration: time)
  end

  #config validation if start_time less to stop_time

end
