# == Schema Information
#
# Table name: activities
#
#  id          :integer          not null, primary key
#  description :string(255)
#  name        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Activity < ApplicationRecord
  has_many :assistants, through: :activity_logs
  has_many :babies, through: :activity_logs
  has_many :activity_logs

  validates :description, :name, presence:true
end
