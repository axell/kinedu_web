# == Schema Information
#
# Table name: babies
#
#  id          :integer          not null, primary key
#  address     :string(255)
#  birthday    :date
#  father_name :string(255)
#  mother_name :string(255)
#  name        :string(255)
#  phone       :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Baby < ApplicationRecord
  has_many :activity_logs
  has_many :activities, through: :activity_logs
  has_many :assistants, through: :activity_logs

  validates :address,:birthday,:father_name,:mother_name,:name,:phone,presence:true

end
