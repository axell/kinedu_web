# == Schema Information
#
# Table name: assistants
#
#  id         :integer          not null, primary key
#  address    :string(255)
#  group      :string(255)
#  name       :string(255)
#  phone      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Assistant < ApplicationRecord
  has_many :activities, through: :activity_logs
  has_many :babies, through: :activity_logs
  has_many :activity_logs

  validates :name,:address,:phone,presence:true
end
