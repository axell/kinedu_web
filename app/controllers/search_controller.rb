class SearchController < ApplicationController
  def create

    baby_id = params[:baby_id]
    assistant_id = params[:assistant_id]
    status = params[:status]

    if status == "Todos" 
      @activity_logs = ActivityLog.where(baby_id: baby_id).where(assistant_id: assistant_id)
    elsif status == "Terminado"
      @activity_logs = ActivityLog.where(baby_id: baby_id).where(assistant_id: assistant_id).where(status: "finished")
    elsif status == "En progreso"
      @activity_logs = ActivityLog.where(baby_id: baby_id).where(assistant_id: assistant_id).where(status: "started")

    end

    respond_to do |format|
      format.js{ render :create,  layout: false, content_type: 'text/javascript' }
    end

  end
end
